package web;

import java.util.ArrayList;
import java.util.List;

import metier.entities.Produit;

public class findProdactModel {
	private String mc;
	private List<Produit> list = new ArrayList<Produit>();

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public List<Produit> getList() {
		return list;
	}

	public void setList(List<Produit> list) {
		this.list = list;
	}

}
