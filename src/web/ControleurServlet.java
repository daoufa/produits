package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import dao.IProduitDao;
import dao.ProduitDaoImpl;
import metier.entities.Produit;

@WebServlet(name = "cs", urlPatterns = { "*.do" })
public class ControleurServlet extends HttpServlet {
	private ProduitDaoImpl prdDao;

	@Override
	public void init() throws ServletException {
		prdDao = new ProduitDaoImpl();
		ApplicationContext springContext=WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
		prdDao=(ProduitDaoImpl) springContext.getBean("dao");
//		prdDao=(ProduitDaoImpl) springContext.getBean(IProduitDao.class);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.err.println("hi");
		String path = request.getServletPath();

		if (path.equals("/index.do")) {

			request.getRequestDispatcher("produits.jsp").forward(request, response);

		} else 
			if (path.equals("/chercher.do")) {

			findProdactModel model = new findProdactModel();
			String mc = request.getParameter("mots cle");
			model.setMc(mc);
			model.setList(prdDao.produitPartMC("%" + mc + "%"));
			request.setAttribute("produits", model);
			request.getRequestDispatcher("produits.jsp").forward(request, response);

		} else 
			if (path.equals("/saisie.do")) {

				request.getRequestDispatcher("saisie.jsp").forward(request, response);

		} else 
			if (path.equals("/saveProduit.do") && request.getMethod().equals("POST")) {
				String designation = request.getParameter("designation");
				double prix = Double.parseDouble(request.getParameter("prix"));
				int quantite = Integer.parseInt(request.getParameter("quantite"));
				Produit p = prdDao.save(new Produit(designation, prix, quantite));
				request.setAttribute("p", p);
				request.getRequestDispatcher("confirmation.jsp").forward(request, response);

		}else 
			if(path.equals("/supprimer.do")) {
				long id=Long.parseLong(request.getParameter("id"));
				prdDao.deleteProduit(id);
				response.sendRedirect("chercher.do?mots+cle=");
			}
			else if(path.equals("/edit.do")){
				long id=Long.parseLong(request.getParameter("id"));
				Produit p=prdDao.getProduit(id);
				request.setAttribute("p", p);
				request.getRequestDispatcher("edit.jsp").forward(request, response);
			}
			else if(path.equals("/SaveChange.do")){
				long id=Long.parseLong(request.getParameter("id"));
				String designation = request.getParameter("designation");
				double prix = Double.parseDouble(request.getParameter("prix"));
				int quantite = Integer.parseInt(request.getParameter("quantite"));
				Produit p=new Produit(designation,prix,quantite);
				p.setId(id);
				prdDao.update(p);
				response.sendRedirect("chercher.do?mots+cle=");
			}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
