package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.entities.Produit;

public class ProduitDaoImpl implements IProduitDao{

	@Override
	public Produit save(Produit p) {
		Connection con=SinglotonConnection.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("insert into produit (designation, prix, quantite) values (?,?,?)");
			ps.setString(1, p.getDesignation());
			ps.setDouble(2, p.getPrix());
			ps.setInt(3, p.getQuantite());
			
			ps.executeUpdate();
			
			PreparedStatement pid=con.prepareStatement("select max(id) as max_id from produit");
			ResultSet rs=pid.executeQuery();
			if(rs.next()) {
				p.setId(rs.getInt("max_id"));
			}
			ps.close();
			pid.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public List<Produit> produitPartMC(String mc) {
		Connection con=SinglotonConnection.getConnection();
		List<Produit> listProd=new ArrayList<Produit>();
		try {
			PreparedStatement ps=con.prepareStatement("select * from produit where designation like ?");
			ps.setString(1,mc);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				listProd.add(convertToProduit(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listProd;
	}

	@Override
	public Produit getProduit(long id) {
		Connection con=SinglotonConnection.getConnection();
		PreparedStatement pr;
		Produit p=null;
		try {
			pr = con.prepareStatement("select * from produit where id like ?");
			pr.setLong(1, id);
			ResultSet rs=pr.executeQuery();
			if(rs.next()) {
				p=convertToProduit(rs);
			}
			
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public Produit update(Produit p) {
		Connection con=SinglotonConnection.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("update produit set designation=?, prix=?, quantite=? where id=?");
			ps.setString(1, p.getDesignation());
			ps.setDouble(2, p.getPrix());
			ps.setInt(3, p.getQuantite());
			ps.setLong(4, p.getId());
			
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public void deleteProduit(long id) {
		Connection con=SinglotonConnection.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("delete from produit where id like ?");
			
			ps.setLong(1, id);
			
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Produit convertToProduit(ResultSet rs) throws SQLException {
		int id=rs.getInt("id");
		String designation=rs.getString("designation");
		double prix=rs.getDouble("prix");
		int quantite=rs.getInt("quantite");
		Produit p=new Produit(designation,prix,quantite);
		p.setId(id);
		return p;
	}

}
