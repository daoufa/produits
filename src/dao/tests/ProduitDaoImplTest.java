package dao.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dao.ProduitDaoImpl;
import metier.entities.Produit;

public class ProduitDaoImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		testDelete();
	}
	public void testSave() {
		ProduitDaoImpl pm=new ProduitDaoImpl();
		Produit p=new Produit("PC portabel assus 2020", 8000, 3);
		Produit result=pm.save(p);
		assertEquals(p.getDesignation(),result.getDesignation());
		assertEquals(p.getPrix(), result.getPrix(),0);
		assertEquals(p.getQuantite(),result.getQuantite());
	}
	
	public void testProduitPartMC() {
		ProduitDaoImpl pm=new ProduitDaoImpl();
		List<Produit> list=pm.produitPartMC("%PC%");
		for(Produit p:list) {
			System.out.println(p);
		}
	}
	
	public void testGetProduit() {
		ProduitDaoImpl pm=new ProduitDaoImpl();
		Produit p=pm.getProduit(1);
		System.out.println(p);
		assertTrue(p.getId()==1 && p.getDesignation().contentEquals("PC portabel assus 2020")&&p.getPrix()==8000.0&&p.getQuantite()==3);
	}
	
	public void testUpdate() {
		ProduitDaoImpl pm=new ProduitDaoImpl();
		Produit p=new Produit("imprimante HD", 5000, 10);
		p.setId(1);
		pm.update(p);
		Produit result=pm.getProduit(1);
		assertEquals(p.getDesignation(),result.getDesignation());
		assertEquals(p.getPrix(), result.getPrix(),0);
		assertEquals(p.getQuantite(),result.getQuantite());
	}
	
	public void testDelete() {
		ProduitDaoImpl pm=new ProduitDaoImpl();
		pm.deleteProduit(3);
	}
}


































