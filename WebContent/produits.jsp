<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>produits</title>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --%>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>
<body>
<%@include file="header.jsp"%>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">rechercher les produits</div>
			<div class="panel-body">
				<form action="chercher.do" type="get">
					<label>Mots cle</label> <input type="text" name="mots cle" value="${produits.mc }"/>
					<button type="submit" class="btn btn-primary">chercher</button>
				</form>
				<table class="table table-striped">
					<tr>
						<th>ID</th>
						<th>DESIGNATION</th>
						<th>PRIX</th>
						<th>QUANTITE</th>
					</tr>
					<c:forEach items="${produits.list }" var="p">
						<tr>
						<c:set var="produit" value="${p }" scope="request"/>
						<td>${p.id }</td>
						<td>${p.designation }</td>
						<td>${p.prix }</td>
						<td>${p.quantite }</td>
						<td><a href="supprimer.do?id=${p.id }">suprimer</a></td>
						<td><a href="edit.do?id=${p.id }">edit</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
</body>
</html>