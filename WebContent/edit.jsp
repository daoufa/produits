<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>produits</title>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --%>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container col-md-6 col-md-offset-3">
		<div class="panel panel-primary">
			<div class="panel-heading">Edit Produit</div>
			<div class="panel-body">
				<form action="SaveChange.do" method="post">
					<div class="table-responsive text-nowrap">
						<table class="table table-striped">
							<tr>
								<td><label>Id</label></td>
								<td><input type="text" name="id" value="${p.id }" /></td>
							</tr>
							<tr>
								<td><label>designation</label></td>
								<td><input type="text" name="designation" value="${p.designation }" /></td>
							</tr>
							<tr>
								<td><label>prix</label></td>
								<td><input type="text" name="prix" value="${p.prix }" /></td>
							</tr>
							<tr>
								<td><label>quantite</label></td>
								<td><input type="text" name="quantite" value="${p.quantite }" /></td>
							</tr>
							<tr>
								<td><label></label></td>
								<td><button type="submit" class="btn btn-primary">ok</button></td>
							</tr>
						</table>
					</div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>