<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>produits</title>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --%>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container col-md-6 col-md-offset-3">
		<div class="panel panel-primary">
			<div class="panel-heading">le produit a bien a jouter</div>
			<div class="panel-body">

				<div class="table-responsive text-nowrap">
					<table class="table table-striped">
						<tr>
							<td><label>ID</label></td>
							<td><label>${p.id }</label></td>
						</tr>
						<tr>
							<td><label>designation</label></td>
							<td><label>${p.designation }</label></td>
						</tr>
						<tr>
							<td><label>prix</label></td>
							<td><label>${p.prix }</label></td>
						</tr>
						<tr>
							<td><label>quantite</label></td>
							<td><label>${p.quantite }</label></td>
						</tr>
					</table>
				</div>

			</div>
		</div>
	</div>
</body>
</html>